"use strict";
    $(document).ready(function() {
        /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const gCOMBO_SMALL = "S";
        const gCOMBO_MEDIUM = "M";
        const gCOMBO_LARGE = "L";
        const gPIZZA_HAWAII = "Hawaii";
        const gPIZZA_SEAFOOD = "seafood";
        const gPIZZA_BACON = "Bacon";
        // mảng cục bộ lưu thông tin các gói combo
        var gComboSmallObj = [];
        var gComboMediumObj = [];
        var gComboLargeObj = [];
        /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        // gán sự kiện cho thẻ body
        onPageLoading();

        // gán sự kiện cho các nút chọn combo
        $("#btn-combo-small").click(function() { // combo small
            onBtnComboSmallClick();
        });
        $("#btn-combo-medium").click(function() { // combo medium
            onBtnComboMediumClick();
        });
        $("#btn-combo-large").click(function() {// combo large
            onBtnComboLargeClick();
        });

        // gán sự kiện cho các nút chọn pizza
        $("#btn-pizza-seafood").click(function() {// pizza seafood
            onBtnPizzaSeafoodClick();
        });
        $("#btn-pizza-hawaii").click(function() {// pizza hawaii
            onBtnPizzaHawaiiClick();
        });
        $("#btn-pizza-bacon").click(function() {// pizza bacon
            onBtnPizzaBaconClick();
        });
        // gán sự kiện cho nút gửi đơn
        $("#btn-send-order").click(function() {
            onBtnSendClick();
        });
        // gán sự kiện cho thẻ select element chọn nước uống
        $('#select-nuoc-uong').on('change', function() {
            console.log("Nước uống được chọn: " + this.value);
        });
        // Gán sự kiện cho nút tạo đơn-modal
        $("#btn-confirm-modal").click(function() {
            onBtnConfirmOrderClick();
        });
        /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        // Hàm xử lý khi tải trang
        function onPageLoading() {
            "use strict";
            console.log("load trang tại đây");
            // gọi hàm lấy dữ liệu nước uống từ server và đổ vào select element
            getDataDrinkObj();
        }
        // Hàm xử lý khi nhấn nút chọn combo small
        function onBtnComboSmallClick() {
            "use strict";
            //tạo một đối tượng gói dịch vụ, được tham số hóa
            gComboSmallObj = getDataCombo(gCOMBO_SMALL, 20, 2, 200, 2, 150000);
            // Hiển thị gói thông tin ra console
            gComboSmallObj.displayInConsole();
            // đổi màu button
            changeColorButton(gCOMBO_SMALL);
        }
        // Hàm xử lý khi nhấn nút chọn combo medium
        function onBtnComboMediumClick() {
            "use strict";
            gComboMediumObj= getDataCombo(gCOMBO_MEDIUM, 25, 4, 300, 3, 200000);
            // Hiển thị gói thông tin ra console
            gComboMediumObj.displayInConsole();
            // đổi màu button
            changeColorButton(gCOMBO_MEDIUM);
        }
        // Hàm xử lý khi nhấn nút chọn combo large
        function onBtnComboLargeClick() {
            "use strict";
            gComboLargeObj = getDataCombo(gCOMBO_LARGE, 30, 8, 500, 4, 250000);
            // Hiển thị gói thông tin ra console
            gComboLargeObj.displayInConsole();
            // đổi màu button
            changeColorButton(gCOMBO_LARGE);
        }
        // Hàm xử lý khi chọn pizza seafood
        function onBtnPizzaSeafoodClick() {
            "use strict";
            var vPizzaDuocChon = getDataPizza(gPIZZA_SEAFOOD);
            // Hiển thị gói thông tin ra console
            vPizzaDuocChon.displayInConsole();
            // đổi màu button
            changeColorButton(gPIZZA_SEAFOOD);
        }
        // Hàm xử lý khi chọn pizza hawaii
        function onBtnPizzaHawaiiClick() {
            "use strict";
            var vPizzaDuocChon = getDataPizza(gPIZZA_HAWAII);
            // Hiển thị gói thông tin ra console
            vPizzaDuocChon.displayInConsole();
            // đổi màu button
            changeColorButton(gPIZZA_HAWAII);
        }
        // Hàm xử lý khi chọn pizza hawaii
        function onBtnPizzaBaconClick() {
            "use strict";
            var vPizzaDuocChon = getDataPizza(gPIZZA_BACON);
            // Hiển thị gói thông tin ra console
            vPizzaDuocChon.displayInConsole();
            // đổi màu button
            changeColorButton(gPIZZA_BACON);
        }
        // Hàm xử lý khi nhấn nút gửi
        function onBtnSendClick() {
            "use strict";
            
            //B1: Thu thập dữ liệu order
            var vOrderInfoObj = getDataOrder();
            //B2: Kiểm tra dữ liệu
            var vValidate = validatedData(vOrderInfoObj);
            if(vValidate == true) {
                // Hàm load dữ liệu order vào modal tạo đơn hàng
                loadDataToModalCreateOrder(vOrderInfoObj);
            }
            
        }
        // Hàm xử lý khi nút xác nhận tạo đơn trên modal được ấn
        function onBtnConfirmOrderClick() {
            "use strict";
            //B1: Thu thập dữ liệu order
            var vOrderInfoObj = getDataOrder();
            // Biến lưu đối tượng dữ liệu order để gửi server tạo đơn hàng
            var vOrderDetailObj = {
                    kichCo: vOrderInfoObj.menuDuocChon.menuName,
                    duongKinh: vOrderInfoObj.menuDuocChon.duongKinhCM,
                    suon: vOrderInfoObj.menuDuocChon.suonNuong,
                    salad: vOrderInfoObj.menuDuocChon.saladGr,
                    loaiPizza: vOrderInfoObj.loaiPizza,
                    idVourcher: vOrderInfoObj.voucher,
                    idLoaiNuocUong: vOrderInfoObj.loaiNuocUong,
                    soLuongNuoc: vOrderInfoObj.menuDuocChon.drink,
                    hoTen: vOrderInfoObj.hoVaTen,
                    thanhTien: vOrderInfoObj.menuDuocChon.priceVND,
                    email: vOrderInfoObj.email,
                    soDienThoai: vOrderInfoObj.dienThoai,
                    diaChi: vOrderInfoObj.diaChi,
                    loiNhan: vOrderInfoObj.loiNhan
                };
            //B2: Kiểm tra dữ liệu (không cần)
            //B3: Gọi server tạo đơn hàng và xử lý hiển thị
            createOrderToServer(vOrderDetailObj);
        }
        /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
        // Hàm thu thập dữ liệu(lấy thông tin combo)
        function getDataCombo( paramMenu, paramDuongKinh, paramSuonNuong, paramSalad, paramDrink, paramPriceVND) {
            "use strict";
            var vComboObj = {
                menuName: paramMenu,
                duongKinhCM: paramDuongKinh,
                suonNuong: paramSuonNuong,
                saladGr: paramSalad,
                drink: paramDrink,
                priceVND: paramPriceVND,
                // Phương thức (method) display in console
                displayInConsole() {
                console.log("%c---Combo được chọn ---", "color: orange");
                console.log("Kích cỡ: " + vComboObj.menuName);
                console.log("Đường kinh: " + vComboObj.duongKinhCM + "cm");
                console.log("Sườn nướng: " + vComboObj.suonNuong);
                console.log("Salad: " + vComboObj.saladGr + "g");
                console.log("Nước ngọt: " + vComboObj.drink);
                console.log("Giá: " + "VND " + vComboObj.priceVND);
                }
            }
            return vComboObj;
        }
        // Hàm thu thập lấy thông tin loại pizza
        function getDataPizza(paramPizza) {
            "use strict";
            var vPizzaObj = {
            pizzaDuocChon: paramPizza,
            displayInConsole() {
                console.log("Pizza được chọn: " + vPizzaObj.pizzaDuocChon);
            }
            }
            return vPizzaObj;
        }
        // Hàm gọi server lấy danh sách nước uống
        function getDataDrinkObj() {
            "use strict";
            $.ajax({
                url: 'http://203.171.20.210:8080/devcamp-pizza365/drinks',
                method: 'GET',
                dataType: 'json',
                async: false,
                success: function(drinkObj) {
                    // hàm load dữ liệu drink vào select element
                    loadDataDrinkToSelectElement(drinkObj);
                },
                error: function(ajaxErro) {
                    alert(ajaxErro.responseText);
                }
            })
        }
        // Hàm load dữ liệu nước uống vào select
        function loadDataDrinkToSelectElement(paramDrinkObj) {
            "use strict";
            var vSelectDrink = $("#select-nuoc-uong");
            for (var bI = 0; bI < paramDrinkObj.length; bI++) {
                var bDrinktOption = $("<option/>", {
                    text: paramDrinkObj[bI].tenNuocUong,
                    value: paramDrinkObj[bI].maNuocUong
                }).appendTo(vSelectDrink);
            }
        }
        // Hàm thu thập dữ liệu đơn hàng
        function getDataOrder() {
            "use strict";
            // truy vấn nút chọn combo
            var vComboSmallSelected = $("#btn-combo-small").attr("data-is-selected-menu");
            var vComboMediumSelected = $("#btn-combo-medium").attr("data-is-selected-menu");
            var vComboLargeSelected = $("#btn-combo-large").attr("data-is-selected-menu");
            var vComboSelected = getDataCombo("", 0, 0 , 0, 0, 0); // Biến lưu combo được chọn
            if(vComboSmallSelected === "Y") {
            vComboSelected = getDataCombo(gCOMBO_SMALL, 20, 2, 200, 2, 150000);
            }
            else if(vComboMediumSelected === "Y") {
            vComboSelected = getDataCombo(gCOMBO_MEDIUM, 25, 4, 300, 3, 200000);
            }
            else if(vComboLargeSelected === "Y") {
            vComboSelected = getDataCombo(gCOMBO_LARGE, 30, 8, 500, 4, 250000);
            }
            // truy vấn nút chọn Pizza
            var vPizzaHaiwaii = $("#btn-pizza-hawaii").attr("data-is-selected-pizza");
            var vPizzaHaiSan = $("#btn-pizza-seafood").attr("data-is-selected-pizza");
            var vPizzaBacon = $("#btn-pizza-bacon").attr("data-is-selected-pizza");
            var vPizzaSelected = ""; // biến lưu pizza được chọn
            if(vPizzaHaiwaii === "Y") {
            vPizzaSelected = gPIZZA_HAWAII;
            }
            else if(vPizzaHaiSan === "Y") {
            vPizzaSelected = gPIZZA_SEAFOOD;
            }
            else if(vPizzaBacon === "Y") {
            vPizzaSelected = gPIZZA_BACON;
            }
            // truy vấn các phần tử input trên form
            var vSelectDrinkValue = $("#select-nuoc-uong").val();
            var vInputFullNameValue = $.trim($("#inp-ho-va-ten").val());
            var vInputEmailValue = $.trim($("#inp-email").val());
            var vInputSoDienThoaiValue = $.trim($("#inp-so-dien-thoai").val());
            var vInputDiaChiValue = $.trim($("#inp-dia-chi").val());
            var vInputMaGiamGia = $.trim($("#inp-ma-giam-gia").val());
            var vLoiNhanValue = $.trim($("#inp-loi-nhan").val());

            // Định nghĩa đối tượng lưu trữ thông tin đơn hàng
            var vOrderObj = {
            menuDuocChon: vComboSelected,
            loaiPizza: vPizzaSelected,
            loaiNuocUong: vSelectDrinkValue,
            hoVaTen: vInputFullNameValue,
            email: vInputEmailValue,
            dienThoai: vInputSoDienThoaiValue,
            diaChi: vInputDiaChiValue,
            voucher: vInputMaGiamGia,
            loiNhan: vLoiNhanValue,
            phanTramGiamGia: getVoucherObj(),
            priceAnnualVND: function() {
                return this.menuDuocChon.priceVND * (100 - getVoucherObj()) / 100;
            }
            };
            return vOrderObj;
        }
        // Hàm lấy giá trị phần trăm giảm giá
        function getVoucherObj() {
            // lấy giá trị trên form mã giảm giá
            var vInputMaGiamGia = $.trim($("#inp-ma-giam-gia").val());
            var vVoucherDiscountPercent = 0; // biến lưu phần trăm giảm giá
            // tạo ra đối tượng request và gửi đi
            if( vInputMaGiamGia != "") {
                $.ajax({
                    url: "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + vInputMaGiamGia,
                    method: 'GET',
                    dataType: 'json',
                    async: false,
                    success: function(maGiamGiaObj) {
                        vVoucherDiscountPercent = maGiamGiaObj.phanTramGiamGia;
                    }
                });
            }
            return vVoucherDiscountPercent;
        }
        // Hàm kiểm tra dữ liệu đơn hàng nhập vào
        function validatedData(paramOrder) { 
            if (paramOrder.menuDuocChon.menuName === "") {
            alert("Chọn combo menu...");
            return false;
            }
            if (paramOrder.loaiPizza === "") {
            alert("Chọn loại pizza...");
            return false;
            }
            if (paramOrder.loaiNuocUong === "all") {
            alert("Chưa chọn đồ uống");
            return false;
            }
            if (paramOrder.hoVaTen === "") {
            alert("Nhập họ và tên");
            return false;
            }
            if (paramOrder.email === "") {
            alert("Chưa nhập email");
            return false;
            }
            if (validateEmail(paramOrder.email) == false) {
            alert("Không phải email");
            return false;
            }
            if (paramOrder.dienThoai === "") {
            alert("Nhập vào số điện thoại...");
            return false;
            }
            if (validatePhone(paramOrder.dienThoai) == false ) {
            alert("Số điện thoại không đúng định dạng");
            return false;
            }
            if (paramOrder.diaChi === "") {
            alert("Địa chỉ không để trống...");
            return false;
            }
            return true;
        }
        // hàm kiểm tra định dạng số điện thoại
        function validatePhone(number) {
            "use strict";
            var vformPhone = /(((\+|)84)|0)(3|5|7|8|9)+([0-9]{8})\b/;
            if (vformPhone.test(number)) {
            return true;
            }
            else {
            return false;
            }
        }
        // hàm kiểm tra định dạng email
        function validateEmail(paramEmail) {
            var atposition = paramEmail.indexOf("@");
            var dotposition = paramEmail.lastIndexOf(".");
            if (atposition < 1 || dotposition < (atposition + 2) || (dotposition + 2) >= paramEmail.length) {
            return false;
            }
            return true;
        }
        //Hàm đổi màu button khi ấn
        function changeColorButton(paramButton) {
            "use strict";
            // truy vấn các phần tử button chọn combo
            var vBtnComboSmall = $("#btn-combo-small");
            var vBtnComboMedium = $("#btn-combo-medium");
            var vBtnComboLarge = $("#btn-combo-large");
            if(paramButton === gCOMBO_SMALL) {
                vBtnComboSmall
                .css({'background':'green'})
                .attr("data-is-selected-menu", "Y");
                vBtnComboMedium
                .css({'background':'orange'})
                .attr("data-is-selected-menu", "N");
                vBtnComboLarge
                .css({'background':'orange'})
                .attr("data-is-selected-menu", "N");
            }
            if(paramButton === gCOMBO_MEDIUM) {
                vBtnComboSmall
                .css({'background':'orange'})
                .attr("data-is-selected-menu", "N");
                vBtnComboMedium
                .css({'background':'green'})
                .attr("data-is-selected-menu", "Y");
                vBtnComboLarge
                .css({'background':'orange'})
                .attr("data-is-selected-menu", "N");
            }
            if(paramButton === gCOMBO_LARGE) {
                vBtnComboSmall
                .css({'background':'orange'})
                .attr("data-is-selected-menu", "N");
                vBtnComboMedium
                .css({'background':'orange'})
                .attr("data-is-selected-menu", "N");
                vBtnComboLarge
                .css({'background':'green'})
                .attr("data-is-selected-menu", "Y");
            }
            // truy vấn phần tử button chọn loại pizza
            var vBtnPizzaSeafood = $("#btn-pizza-seafood");
            var vBtnPizzaHawaii = $("#btn-pizza-hawaii");
            var vBtnPizzaBacon = $("#btn-pizza-bacon");
            if(paramButton === gPIZZA_SEAFOOD) {
                vBtnPizzaSeafood
                .css({'background':'green'})
                .attr("data-is-selected-pizza", "Y");
                vBtnPizzaHawaii
                .css({'background':'orange'})
                .attr("data-is-selected-pizza", "N");
                vBtnPizzaBacon
                .css({'background':'orange'})
                .attr("data-is-selected-pizza", "N");
            }
            if(paramButton === gPIZZA_HAWAII) {
                vBtnPizzaSeafood
                .css({'background':'orange'})
                .attr("data-is-selected-pizza", "N");
                vBtnPizzaHawaii
                .css({'background':'green'})
                .attr("data-is-selected-pizza", "Y");
                vBtnPizzaBacon
                .css({'background':'orange'})
                .attr("data-is-selected-pizza", "N");
            }
            if(paramButton === gPIZZA_BACON) {
                vBtnPizzaSeafood
                .css({'background':'orange'})
                .attr("data-is-selected-pizza", "N");
                vBtnPizzaHawaii
                .css({'background':'orange'})
                .attr("data-is-selected-pizza", "N");
                vBtnPizzaBacon
                .css({'background':'green'})
                .attr("data-is-selected-pizza", "Y");
            }
        }
        // Hàm load dữ liệu order sau khi nhấn nút gửi
        function loadDataToModalCreateOrder(paramOrder) {
            "use strict";
            // hiện modal tạo đơn hàng
            $("#modal-create-order").modal('show');
            var vSelectDrinkText =  $("#select-nuoc-uong option:selected").text(); // lấy giá trị text trên select element
            if (getVoucherObj() == 0) {
            alert("Không có voucher");
            paramOrder.voucher = "không có mã giảm giá";
            };
            // gán dữ liệu vào các trường trên form
            $("#inp-ho-va-ten-modal").val(paramOrder.hoVaTen);
            $("#inp-so-dien-thoai-modal").val(paramOrder.dienThoai);
            $("#inp-dia-chi-modal").val(paramOrder.diaChi);
            $("#inp-loi-nhan-modal").val(paramOrder.loiNhan);
            $("#inp-ma-giam-gia-modal").val(paramOrder.voucher);
            $("#textarea-info-detail")
                .val(
                    "Xác nhận: " + paramOrder.hoVaTen + ", " + paramOrder.dienThoai + ", " + paramOrder.diaChi
                    + "\nMenu " + paramOrder.menuDuocChon.menuName + ", sườn nướng " + paramOrder.menuDuocChon.suonNuong + ", tên nước uống " + vSelectDrinkText + ", số lượng nước uống " + paramOrder.menuDuocChon.drink + ", salad " + paramOrder.menuDuocChon.saladGr + "g"
                    + "\nLoại pizza: " + paramOrder.loaiPizza + ", Giá: " + paramOrder.menuDuocChon.priceVND + "VNĐ " + ", Mã giảm giá: " + paramOrder.voucher
                    + "\nPhải thanh toán: " + paramOrder.priceAnnualVND() + "VNĐ" +  " (giảm giá " + paramOrder.phanTramGiamGia + "%)"
                    );
        }
        // Hàm gọi server request tạo đơn hàng
        function createOrderToServer(paramOrderObj) {
            "use strict";
            $.ajax({
                url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
                method: 'POST',
                data: JSON.stringify(paramOrderObj),
                contentType: 'application/json;charset=UTF-8',
                success: function(reponseOrderObj) {
                    alert("Tạo đơn thành công");
                    console.log(reponseOrderObj);          
                    // ghi mã đơn hàng ra modal kết quả
                    loadOrderCodeToModal(reponseOrderObj);
                },
                error: function(ajaxErro) {
                    alert(ajaxErro.responseText);
                }
            })
        }
        // Hàm load mã đơn hàng vào modal kết quả
        function loadOrderCodeToModal(paramOrderObj) {
            "use strict";
            // ẩn modal chi tiết đơn hàng
            $("#modal-create-order").modal('hide');
            // hiện modal hiển thị kết quả
            $("#modal-confirm-order").modal('show');
            // load dữ liệu mã đơn hàng vào trường input
            $("#inp-ma-don-hang").val(paramOrderObj.orderCode);
        }
    });